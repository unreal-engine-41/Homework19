﻿# include <iostream>

class Animal
{
public:
	Animal() {}
	
	virtual void Show() = 0;
};

class Dog : public Animal
{
public:
	Dog() {}
	
	void Show() override
	{
		std::cout << "Gaw ";
	}
};

class Cat : public Animal
{
public:
	Cat() {}

	void Show() override
	{
		std::cout << "May ";
	}
};

class Cow : public Animal
{
public:
	Cow() {}

	void Show() override
	{
		std::cout << "Myy ";
	}
};

int main()
{
	Cat* p1 = new Cat();
	Dog* p2 = new Dog();
	Cow* p3 = new Cow();

	Animal* arr[3] = { p3 ,p2,p1};
	for (auto element : arr)
	{
		element->Show();
	}

}
